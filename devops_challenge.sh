#!/bin/bash
#
# AWS S3 Buckets Analytics
# It will give the Bucket name, Bucket region, total files, bucket size, 
# bucket creation date and time, bucket latest file modification date and time 
# Supported Operating system's: Redhat, Ubuntu, Centos
#
#Usage insructions:
#AWS-Cli must be installed and need account AWS configuration 
#user must have Cost exploer privileges 
#Requires aws, s3api, IAM account must have permission to access all buckets
#For getting price of S3 service you need to pass few arguments through json file for aws ce command line 
#set -x

if [ $# -gt 1 ]
 then
   echo "Too many arguments   Usage: $0 path/to/arguments json file"
   exit 1
 elif [ $# -lt 1 ]
   then
     echo "Need to pass arguments  Usage: $0 path/to/arguments json file"
     exit 1
fi


#cheking AWS-cli installed or not...?
type -P aws &>/dev/null

if [ $? -ne 0 ]
then
   echo "Unable to find aws cli, please install it and run this script again."
   exit 1
fi


# Verify AWS CLI Credentials are setup
if ! grep -q aws_access_key_id ~/.aws/config; then
  if ! grep -q aws_access_key_id ~/.aws/credentials; then
    echo "AWS config not found. Please run \"aws configure\"."
    exit 1
  fi
  if [ ! -e $HOME/.aws/config ]
  then
      echo "AWS config file missed check once in .aws directory"
      exit 1
  fi  
fi
echo "Found AWS Configuration, Searching for S3 Buckets, It will take some time..."

#checking AWS configured properly or not...?
aws s3 ls &>/dev/null

if [ $? -ne 0 ]
then
    echo "Looks like problem with aws configuration, check once wether you have right configuration or not...?"
    exit 1
fi

#getting bucket name
bucket_name=($(aws s3 ls | awk '{print $3}'))
#getting bucket creation date
bucket_creation_date=($(aws s3 ls | awk '{print $1}'))
#getting bucket creation time
bucket_creation_time=($(aws s3 ls | awk '{print $2}'))

j=1
k=0

for i in "${bucket_name[@]}"
  do
      echo ""
      echo "${j}) S3 Bucket Name: ${i}"
	  #getting bucket region
      bucket_region=$(aws s3api get-bucket-location --bucket ${i} --output text)
      echo "Bucket Region: ${bucket_region}"
      #getting total objects and size of bucket
	  aws s3 ls s3://${i} --recursive --human-readable --summarize | sort | tail -n 2 | awk '{$1=$1;print}'
	  bucket_last_modified_date=$(aws s3 ls ${i} --recursive | sort | tail -n 1 | cut -d ' ' -f1,2 )
      echo "Buckket creation date: ${bucket_creation_date[${k}]}"
      echo "Buckket creation time: ${bucket_creation_time[${k}]}"
      echo "Bucket Last modified date & time: ${bucket_last_modified_date}"
      echo ""
      k=$(( ${k}+ 1 ))
      j=$(( ${j}+ 1 ))
  done

echo "Total Number of Buckets: $(( ${j}-1))"
#Getting cost of S3 service
aws ce get-cost-and-usage --cli-input-json file://$1 --output text | grep BLENDEDCOST | tail -n2
#Time period of S3 service cost 
aws ce get-cost-and-usage --cli-input-json file://$1 --output text | tail -n1
echo ""

